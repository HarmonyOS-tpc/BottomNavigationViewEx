package com.example.entry.slice;

import com.example.entry.BadgeViewAbility;
import com.example.entry.ResourceTable;
import com.example.entry.StytlyAbility;
import com.example.entry.WithViewPagerAbility;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private Button btn_style;
    private Button btn_with_view_pager;
    private Button btn_badge_view;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        btn_style = (Button) findComponentById(ResourceTable.Id_btn_style);
        btn_with_view_pager = (Button) findComponentById(ResourceTable.Id_btn_with_view_pager);
        btn_badge_view = (Button) findComponentById(ResourceTable.Id_btn_badge_view);

        btn_style.setClickedListener(this);
        btn_with_view_pager.setClickedListener(this);

        btn_badge_view.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_style:
                startAbility(getBundleName(), StytlyAbility.class.getName());
                break;

            case ResourceTable.Id_btn_with_view_pager:
                startAbility(getBundleName(), WithViewPagerAbility.class.getName());
                break;

            case ResourceTable.Id_btn_badge_view:
                startAbility(getBundleName(), BadgeViewAbility.class.getName());
                break;

            default:
                break;
        }
    }

    private void startAbility(String bundleName, String toAbilityName) {
        Intent intent = new Intent();
        Operation operation =
                new Intent.OperationBuilder().withBundleName(bundleName).withAbilityName(toAbilityName).build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}
