package com.example.entry.slice;

import com.example.entry.ResourceTable;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.ittianyu.bottomnavigationviewex.item.BottomNavigationItem;
import com.ittianyu.bottomnavigationviewex.item.ShapeBadgeItem;
import com.ittianyu.bottomnavigationviewex.item.TextBadgeItem;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AttrHelper;

public class BadgeViewAbilitySlice extends AbilitySlice {
    private BottomNavigationViewEx abt_badge_bnve;
    private TextBadgeItem numberBadgeItem;
    private ShapeBadgeItem shapeBadgeItem;

    private int shapeBadge = ShapeBadgeItem.SHAPE_OVAL;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_badge_view);
        initView();
        refresh();
    }

    private void initView() {
        abt_badge_bnve = (BottomNavigationViewEx) findComponentById(ResourceTable.Id_abt_badge_bnve);
    }

    public void refresh() {
        numberBadgeItem =
                new TextBadgeItem()
                        .setBorderWidth(AttrHelper.vp2px(1.5f, getContext()))
                        .setBackgroundColor(0xffff5454)
                        .setText("2")
                        .setHideOnSelect(true);

        shapeBadgeItem =
                new ShapeBadgeItem()
                        .setShape(shapeBadge)
                        .setShapeColor(0xffff5454)
                        .setOffset(AttrHelper.vp2px(20, getContext()), AttrHelper.vp2px(-20, getContext()))
                        .setHideOnSelect(true);

        abt_badge_bnve.clearAll();
        abt_badge_bnve
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_location_on_white_24dp, "音乐", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "云端", getContext()))
                .addItem(
                        new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "收藏", getContext())
                                .setBadgeItem(numberBadgeItem))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "发现", getContext()))
                .addItem(
                        new BottomNavigationItem(ResourceTable.Media_ic_favorite_white_24dp, "朋友圈", getContext())
                                .setBadgeItem(shapeBadgeItem))
                .setCurrentItem(0)
                .setShowAllText(false)
                .initialise();
    }
}
