package com.example.entry.slice;

import com.example.entry.ResourceTable;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.ittianyu.bottomnavigationviewex.item.BottomNavigationItem;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

public class WithViewPagerAbilitySlice extends AbilitySlice {
    private BottomNavigationViewEx btn_view;
    private PageSlider pageSlider;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_with_view_pager);

        btn_view = (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve);
        btn_view.addItem(new BottomNavigationItem(ResourceTable.Media_ic_location_on_white_24dp, "音乐", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "云端", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_favorite_white_24dp, "朋友圈", getContext()))
                .setCurrentItem(0)
                .setShowAllText(false)
                .setBackgroundColor(Color.BLUE.getValue())
                .initialise();
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_vp);
        pageSlider.setProvider(new VpAdapter(getData()));

        btn_view.setTabSelectedListener(
                new BottomNavigationViewEx.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(int position) {
                        switch (position) {
                            case 0:
                                pageSlider.setCurrentPage(0);
                                break;

                            case 1:
                                pageSlider.setCurrentPage(1);
                                break;

                            case 2:
                                pageSlider.setCurrentPage(2);
                                break;

                            default:
                                break;
                        }
                    }

                    @Override
                    public void onTabUnselected(int position) {}

                    @Override
                    public void onTabReselected(int position) {}
                });

        btn_view.setupWithViewPager(pageSlider);
    }

    private ArrayList<DataItem> getData() {
        ArrayList<DataItem> dataItems = new ArrayList<>();
        dataItems.add(new DataItem("音乐"));
        dataItems.add(new DataItem("云端"));
        dataItems.add(new DataItem("朋友圈"));

        return dataItems;
    }

    private class VpAdapter extends PageSliderProvider {
        private List<DataItem> list;

        public VpAdapter(List<DataItem> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int i) {
            final DataItem data = list.get(i);
            Text label = new Text(null);
            label.setTextAlignment(TextAlignment.CENTER);
            label.setLayoutConfig(
                    new StackLayout.LayoutConfig(
                            ComponentContainer.LayoutConfig.MATCH_PARENT,
                            ComponentContainer.LayoutConfig.MATCH_PARENT));
            label.setText(data.mText);
            label.setTextColor(Color.BLACK);
            label.setTextSize(50);
            componentContainer.addComponent(label);
            return label;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
            componentContainer.removeComponent((Component) o);
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object o) {
            return true;
        }

        // 数据实体类

    }

    public static class DataItem {
        String mText;

        public DataItem(String txt) {
            mText = txt;
        }
    }
}
