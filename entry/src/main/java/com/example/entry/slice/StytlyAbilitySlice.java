package com.example.entry.slice;

import com.example.entry.ResourceTable;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.ittianyu.bottomnavigationviewex.item.BottomNavigationItem;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;

import java.net.PortUnreachableException;
import java.util.ArrayList;

public class StytlyAbilitySlice extends AbilitySlice {
    private BottomNavigationViewEx bnve_normal;
    private BottomNavigationViewEx bnve_no_animation;
    private BottomNavigationViewEx bnve_no_shifting_mode;
    private BottomNavigationViewEx bnve_no_item_shifting_mode;

    private BottomNavigationViewEx bnve_no_text;
    private BottomNavigationViewEx bnve_no_icon;
    private BottomNavigationViewEx bnve_no_animation_shifting_mode;
    private BottomNavigationViewEx bnve_no_animation_item_shifting_mode;

    private BottomNavigationViewEx bnve_no_animation_shifting_mode_item_shifting_mode;
    private BottomNavigationViewEx bnve_no_shifting_mode_item_shift_mode_text;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_stytly);

        initView();

        refreshNormal();
        refreshNoAnimation();
        noShiftingMode();
        noItemShiftingMode();

        noText();
        noIcon();
        noAnimationShiftingMode();
        noAnimationItemShiftingMode();

        noAnimationShftingModeItemShiftingMode();
        noShiftingModeItemShiftModeText();
    }

    private void initView() {
        bnve_normal = (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_normal);
        bnve_no_animation = (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_no_animation);
        bnve_no_shifting_mode = (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_no_shifting_mode);
        bnve_no_item_shifting_mode =
                (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_no_item_shifting_mode);

        bnve_no_text = (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_no_text);
        bnve_no_icon = (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_no_icon);
        bnve_no_animation_shifting_mode =
                (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_no_animation_shifting_mode);
        bnve_no_animation_item_shifting_mode =
                (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_no_animation_item_shifting_mode);

        bnve_no_animation_shifting_mode_item_shifting_mode =
                (BottomNavigationViewEx)
                        findComponentById(ResourceTable.Id_bnve_no_animation_shifting_mode_item_shifting_mode);
        bnve_no_shifting_mode_item_shift_mode_text =
                (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_no_shifting_mode_item_shift_mode_text);
    }

    public void refreshNormal() {
        bnve_normal.clearAll();
        bnve_normal
                .addItem( new BottomNavigationItem(ResourceTable.Media_ic_music_note_white_24dp, "音乐", getContext()))
                .addItem( new BottomNavigationItem(ResourceTable.Media_ic_book_white_24dp, "云端", getContext()))
                .addItem( new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "朋友圈", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_favorite_white_24dp, "收藏", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_home_white_24dp, "发现", getContext()))
                .setCurrentItem(0)
                .initialise();
    }

    public void refreshNoAnimation() {
        bnve_no_animation.clearAll();
        bnve_no_animation.enableAnimation(false);
        addItem(bnve_no_animation);
    }

    public void noShiftingMode() {
        bnve_no_shifting_mode.enableShiftingMode(false);
        addItem(bnve_no_shifting_mode);
    }

    public void noItemShiftingMode() {
        bnve_no_item_shifting_mode.setShowAllText(false);
        addItem(bnve_no_item_shifting_mode);
    }

    public void noText() {
        bnve_no_text.setTextVisibility(false);
        addItem(bnve_no_text);
    }

    public void noIcon() {
        bnve_no_icon.setIconVisibility(false);
        addItem(bnve_no_icon);
    }

    public void noAnimationShiftingMode() {
        bnve_no_animation_shifting_mode.enableShiftingMode(false);
        addItem(bnve_no_animation_shifting_mode);
    }

    public void noAnimationItemShiftingMode() {
        bnve_no_animation_item_shifting_mode.setShowAllText(false);
        addItem(bnve_no_animation_item_shifting_mode);
    }

    public void noAnimationShftingModeItemShiftingMode() {
        bnve_no_animation_shifting_mode_item_shifting_mode.setShowAllText(false);
        bnve_no_animation_shifting_mode_item_shifting_mode.enableShiftingMode(false);
        addItem(bnve_no_animation_shifting_mode_item_shifting_mode);
    }

    public void noShiftingModeItemShiftModeText() {
        bnve_no_shifting_mode_item_shift_mode_text.setTextVisibility(false);
        bnve_no_shifting_mode_item_shift_mode_text.enableShiftingMode(false);
        addItem(bnve_no_shifting_mode_item_shift_mode_text);
    }

    private void addItem(BottomNavigationViewEx item) {
        item.addItem(new BottomNavigationItem(ResourceTable.Media_ic_location_on_white_24dp, "音乐", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "云端", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_favorite_white_24dp, "朋友圈", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_launch_white_24dp, "收藏", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_book_white_24dp, "发现", getContext()))
                .setCurrentItem(0)
                .initialise();
    }
}
