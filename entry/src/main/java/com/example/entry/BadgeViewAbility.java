package com.example.entry;

import com.example.entry.slice.BadgeViewAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class BadgeViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(BadgeViewAbilitySlice.class.getName());
    }
}
