## Gradle Dependency
```groovy
dependencies {
    implementation "io.openharmony.tpc.thirdlib:BottomNavigationViewEx:1.0.1"

}
```

## 功能 ##
|addItem|在代码中动态设置 item
|enableAnimation|开启或关闭点击动画。 默认为 true.
|enableItemShiftingMode|开始或关闭子菜单位移模式。
|setBackgroundColor|设置底部栏背景颜色
|setIconVisibility|设置item图标是否显示，如果为 true，除了当前选中项，其他项的图标将会隐藏。
|setTextVisibility|设置item 标签文本是否显示，如果为 true，除了当前选中项，其他项的标签文本将会隐藏。
|setShowAllText|设置所有项 item 标签文本全部显示
|getMenuItemPosition|获取指定的 item 的索引
|setCurrentItem|设置当前选中项。
|setTabSelectedListener|设置菜单的监听事件
|getIconAt|获取指定 item 的图标
|getItemCount|获取菜单中 item 的个数
|getLargeLabelAtText|获取指定item 的标签文本
|setLargeLabelAtText|设置指定item 的标签文本
|getBottomNavigationItemView|获取位于 position 的私有成员变量 mButton。
|getBottomNavigationItemViews|获取私有成员变量 mButtons。
|setupWithViewPager|和 PageSlider  绑定，当 任何一个选中项改变时，都会自动改变另一项。



## 开始使用 ##


在 `xml` 布局中添加自定义控件:
```xml
  <com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx
            ohos:id="$+id:bnve_no_animation_shifting_mode_item_shifting_mode"
            ohos:width="match_parent"
            ohos:height="match_content"
            />
```
在 `java` 代码中添加item:
 bnve_normal = (BottomNavigationViewEx) findComponentById(ResourceTable.Id_bnve_normal);
 bnve_normal.clearAll();
 bnve_normal
            .addItem( new BottomNavigationItem(ResourceTable.Media_ic_music_note_white_24dp, "音乐", getContext()))
            .addItem( new BottomNavigationItem(ResourceTable.Media_ic_book_white_24dp, "云端", getContext()))
            .addItem( new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "朋友圈", getContext()))
            .addItem(new BottomNavigationItem(ResourceTable.Media_ic_favorite_white_24dp, "收藏", getContext()))
            .addItem(new BottomNavigationItem(ResourceTable.Media_ic_home_white_24dp, "发现", getContext()))
            .setCurrentItem(0)
            .initialise();


#### 禁止所有动画效果 ####
```java
bnve_normal.enableAnimation(false);
bnve_normal.enableShiftingMode(false);


#### 和 PageSlider 绑定####
```java
// set Provider
 pageSlider = (PageSlider) findComponentById(ResourceTable.Id_vp);
 pageSlider.setProvider(new VpAdapter(getData()));

// binding with pageSlider
 bnve_normal.setupWithViewPager(pageSlider);
```


#### 添加带数字的小红点 ####
        abt_badge_bnve = (BottomNavigationViewEx) findComponentById(ResourceTable.Id_abt_badge_bnve);
numberBadgeItem =
                new TextBadgeItem()
                        .setBorderWidth(AttrHelper.vp2px(1.5f, getContext()))
                        .setBackgroundColor(0xffff5454)
                        .setText("2")
                        .setHideOnSelect(true);

        shapeBadgeItem =
                new ShapeBadgeItem()
                        .setShape(shapeBadge)
                        .setShapeColor(0xffff5454)
                        .setOffset(AttrHelper.vp2px(20, getContext()), AttrHelper.vp2px(-20, getContext()))
                        .setHideOnSelect(true);

        abt_badge_bnve.clearAll();
        abt_badge_bnve
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_location_on_white_24dp, "音乐", getContext()))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "云端", getContext()))
                .addItem(
                        new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "收藏", getContext())
                                .setBadgeItem(numberBadgeItem))
                .addItem(new BottomNavigationItem(ResourceTable.Media_ic_find_replace_white_24dp, "发现", getContext()))
                .addItem(
                        new BottomNavigationItem(ResourceTable.Media_ic_favorite_white_24dp, "朋友圈", getContext())
                                .setBadgeItem(shapeBadgeItem))
                .setCurrentItem(0)
                .setShowAllText(false)
                .initialise();

## 授权 ##

	MIT License

	Copyright (c) 2017 ittianyu

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

