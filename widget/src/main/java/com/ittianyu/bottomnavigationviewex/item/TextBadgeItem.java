package com.ittianyu.bottomnavigationviewex.item;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;

public class TextBadgeItem extends BadgeItem<TextBadgeItem> {
    private int mBackgroundColor = Color.RED.getValue();
    private int mTextColor = Color.WHITE.getValue();

    private String mText;
    private int mBorderColor = Color.WHITE.getValue();
    private int mBorderWhidthInPixels = 0;
    private float radius = -1;
    private Paint mTextPaint = new Paint();
    private Paint mBorderPaint = new Paint();
    private Paint mBackgroundPaint = new Paint();

    @Override
    TextBadgeItem getSubInstance() {
        return this;
    }

    public boolean getHideOnSelect() {
        return mHideOnSelect;
    }

    public TextBadgeItem setBackgroundColor(int backgroundColor) {
        mBackgroundColor = backgroundColor;
        return this;
    }

    public TextBadgeItem setTextColor(int color) {
        mTextColor = color;
        return this;
    }

    public TextBadgeItem setText(String text) {
        mText = text;
        return this;
    }

    public TextBadgeItem setBorderColor(int color) {
        mBorderColor = color;
        return this;
    }

    public TextBadgeItem setCornerRadius(int radius) {
        this.radius = radius;
        return this;
    }

    public TextBadgeItem setBorderWidth(int borderWidthInPixels) {
        this.mBorderWhidthInPixels = borderWidthInPixels;
        return this;
    }

    @Override
    public void drawToCanvas(
            Canvas canvas,
            float centerX,
            float centerY,
            float defaultOffsetX,
            float defaultOffsetY,
            int size,
            int padding) {
        if (!isSetOffset) {
            offsetX = defaultOffsetX;
            offsetY = defaultOffsetY;
        }
        mTextPaint.setAntiAlias(true);
        mTextPaint.setTextSize(size);
        mTextPaint.setColor(new Color(mTextColor));
        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setStrokeWidth(mBorderWhidthInPixels);
        mBorderPaint.setColor(new Color(mBorderColor));
        mBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setColor(new Color(mBackgroundColor));

        float textWidth = mTextPaint.measureText(mText);
        float textHeight = mTextPaint.descent() - mTextPaint.ascent();

        RectFloat rectFloat = new RectFloat();
        if (textWidth > textHeight) {
            rectFloat.left = centerX + offsetX - textWidth / 2 - padding;
            rectFloat.right = centerX + offsetX + textWidth / 2 + padding;
        } else {
            rectFloat.left = centerX + offsetX - textHeight / 2 - padding;
            rectFloat.right = centerX + offsetX + textHeight / 2 + padding;
        }
        rectFloat.top = centerY + offsetY - textHeight / 2 - padding;
        rectFloat.bottom = centerY + offsetY + textHeight / 2 + padding;
        if (radius == -1) {
            radius = textHeight / 2 + padding;
        }
        canvas.drawRoundRect(rectFloat, radius, radius, mBackgroundPaint);
        if (mBorderWhidthInPixels > 0) {
            canvas.drawRoundRect(rectFloat, radius, radius, mBorderPaint);
        }
        canvas.drawText(mTextPaint, mText, centerX + offsetX - textWidth / 2, centerY + offsetY + textHeight / 4);
    }
}
