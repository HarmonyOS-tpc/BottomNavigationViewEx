package com.ittianyu.bottomnavigationviewex.item;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.ittianyu.bottomnavigationviewex.utils.ResUtil;

import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.VectorElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.AlphaType;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.util.Optional;

public class BottomNavigationItem {
    private PixelMap orgPixelMap;
    private PixelMap orgInActivePixelMap;

    private PixelMap activePixelMap;
    private PixelMap inActivePixelMap;
    private PixelMap backgroundPixelMap;

    private int iconSize;

    private int mIconResource;
    private Element mElement;

    private int mInactiveIconResource;
    private boolean inActiveIconAvailable = false;
    private String mTitle;

    private int mActiveColor;
    private int mInActiveColor;
    private int mBackgrounColor;

    private BadgeItem badgeItem;
    private BottomNavigationViewEx bottomNavigationBar;

    public BottomNavigationItem(int mIconResource, String mTitle, Context mContext) {
        this.mIconResource = mIconResource;
        this.mTitle = mTitle;
        Optional<PixelMap> optional = ResUtil.getPixelMap(mContext, mIconResource);
        orgPixelMap = optional.isPresent() ? optional.get() : null;
    }

    public BottomNavigationItem(PixelMap pixelMap, String mTitle) {
        this.orgPixelMap = pixelMap;
        this.mTitle = mTitle;
    }

    public BottomNavigationItem setInactiveIconResource(int mInactiveIconResource, Context mContext) {
        this.mInactiveIconResource = mInactiveIconResource;
        Optional<PixelMap> optional = ResUtil.getPixelMap(mContext, mInactiveIconResource);
        orgInActivePixelMap = optional.isPresent() ? optional.get() : null;
        inActiveIconAvailable = true;
        return this;
    }

    // 设置item点击时动画背景色
    public BottomNavigationItem setActiveColor(int color) {
        this.mActiveColor = color;
        return this;
    }

    // 设置item图标未选中时的背景色
    public BottomNavigationItem setInActiveColor(int color) {
        this.mInActiveColor = color;
        return this;
    }

    public String getText() {
        return this.mTitle;
    }

    public void setText(String text) {
        this.mTitle = text;
    }

    public PixelMap getInactiveIcon() {
        return orgPixelMap;
    }

    boolean isInActiveIconAvailable() {
        return inActiveIconAvailable;
    }

    public int getActiveColor() {
        return mActiveColor;
    }

    public int getInActiveColor() {
        return mInActiveColor;
    }

    public int getBackgroundColor() {
        return mBackgrounColor;
    }

    public BottomNavigationItem setBadgeItem(BadgeItem badgeItem) {
        badgeItem.bottomNavigationBar = this.bottomNavigationBar;
        this.badgeItem = badgeItem;
        return this;
    }

    public BadgeItem getBadgeItem() {
        return badgeItem;
    }

    public void setupPixelMap(BottomNavigationViewEx bottomNavigationBar, int iconSize) {
        this.bottomNavigationBar = bottomNavigationBar;
        if (badgeItem != null) {
            badgeItem.bottomNavigationBar = bottomNavigationBar;
        }
        this.iconSize = iconSize;
        this.mBackgrounColor = bottomNavigationBar.getBackgroundColor();
        if (orgPixelMap != null) {
            generateIconBitmaps(bottomNavigationBar);
        }
    }

    public PixelMap getActivePixelMap() {
        return activePixelMap;
    }

    public PixelMap getInActivePixelMap() {
        return inActivePixelMap;
    }

    public PixelMap getBackgroundPixelMap() {
        return backgroundPixelMap;
    }

    public PixelMap copy(PixelMap source, int width, int height) {
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        initializationOptions.alphaType = AlphaType.PREMUL;
        initializationOptions.size = new Size(width, height);
        return PixelMap.create(source, initializationOptions);
    }

    private PixelMap scaleIcon(PixelMap origin) {
        int width = origin.getImageInfo().size.width;
        int height = origin.getImageInfo().size.height;
        int size = Math.max(width, height);
        if (size == iconSize) {
            return origin;
        } else if (size > iconSize) {
            int scaledWidth;
            int scaledHeight;
            if (width > iconSize) {
                scaledWidth = iconSize;
                scaledHeight = (int) (iconSize * ((float) height / width));
            } else {
                scaledHeight = iconSize;
                scaledWidth = (int) (iconSize * ((float) width / height));
            }
            return copy(origin, scaledWidth, scaledHeight);
        } else {
            return origin;
        }
    }

    private void generateIconBitmaps(BottomNavigationViewEx bottomNavigationBar) {
        if (orgPixelMap == null) {
            return;
        }
        orgPixelMap = scaleIcon(orgPixelMap);
        if (inActivePixelMap != null) {
            inActivePixelMap = scaleIcon(inActivePixelMap);
        }
        PixelMap origin = orgPixelMap;
        activePixelMap = copy(origin, origin.getImageInfo().size.width, origin.getImageInfo().size.height);
        Canvas canvas = new Canvas(new Texture(activePixelMap));
        canvas.drawColor(
                mActiveColor != 0 ? mActiveColor : bottomNavigationBar.getActiveColor(), Canvas.PorterDuffMode.SRC_IN);
        if (inActiveIconAvailable && orgInActivePixelMap != null) {
            inActivePixelMap =
                    copy(
                            inActivePixelMap,
                            inActivePixelMap.getImageInfo().size.width,
                            inActivePixelMap.getImageInfo().size.height);
        } else {
            inActivePixelMap = copy(origin, origin.getImageInfo().size.width, origin.getImageInfo().size.height);
            canvas = new Canvas(new Texture(inActivePixelMap));
            canvas.drawColor(
                    mInActiveColor != 0 ? mInActiveColor : bottomNavigationBar.getInActiveColor(),
                    Canvas.PorterDuffMode.SRC_IN);
        }
        backgroundPixelMap = copy(origin, origin.getImageInfo().size.width, origin.getImageInfo().size.height);
        canvas = new Canvas(new Texture(backgroundPixelMap));
        canvas.drawColor(mBackgrounColor, Canvas.PorterDuffMode.SRC_IN);
    }
}
