package com.ittianyu.bottomnavigationviewex.item;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import ohos.agp.render.Canvas;

public abstract class BadgeItem<T extends BadgeItem<T>> {
    boolean isSetOffset = false;
    float offsetX;
    float offsetY;

    boolean mHideOnSelect;

    BottomNavigationViewEx bottomNavigationBar;

    private boolean mIsHidden = false;

    public T toggle() {
        if (mIsHidden) {
            return show();
        } else {
            return hide();
        }
    }

    public T show() {
        mIsHidden = false;
        if (bottomNavigationBar != null) {
            bottomNavigationBar.invalidate();
        }
        return getSubInstance();
    }

    public T hide() {
        mIsHidden = true;
        if (bottomNavigationBar != null) {
            bottomNavigationBar.invalidate();
        }
        return getSubInstance();
    }

    public boolean isHidden() {
        return mIsHidden;
    }

    public T setOffset(float offsetX, float offsetY) {
        isSetOffset = true;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        return getSubInstance();
    }

    public T clearOffset() {
        isSetOffset = false;
        return getSubInstance();
    }

    public boolean isHideOnSelect() {
        return mHideOnSelect;
    }

    public T setHideOnSelect(boolean hideOnSelect) {
        this.mHideOnSelect = hideOnSelect;
        return getSubInstance();
    }

    abstract T getSubInstance();

    public abstract void drawToCanvas(
            Canvas canvas,
            float centerX,
            float centerY,
            float defaultOffsetX,
            float defaultOffsetY,
            int size,
            int padding);
}
